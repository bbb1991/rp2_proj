## Description
This script takes as input folder, that contains a list of jpeg images. And using
following techiniques tries to detect fraud images. These techinques are:
* Analyzing metadatas fromo parsing output of exiftool
* Analyzing output of tool called JPEGSnoop
* Analyzing ELA

Also, this project has tiny script called `file_renamer.py` that bulk renames files from given folder.
For example, files such as:

```
foo.txt
bar.txt
biz.txt
```
will be renamed as 
```
1.txt
2.txt
3.txt
```

## Requirements
* Windows
* Python3
* Virtual environment (Optionally)

## How to run:

1. Create virtual environment
2. Activate virtual environment
3. Install requirements
```
pip install -r requirements.txt
```
4. Run script
```
python start.py --dir /path/to/folder/with/images
```
While script works, it prints to the console useful info, also, in parrallel, 
this info duplicates to file `output.log`.

## Known bugs
1. MemoryError
Because of we using numpy it tries to save all data in memory and may cause 
of exception `MemoryError` we can do nothing, because it is required step.
 - One solution is if you are using 32 bit version of Python and you have more
  than 4Gb RAM then try install 64 bit version of Python. 
  It might help in situations, if you have more than 4Gb RAM
 
2. Error "FileName encoding not specified.  Use -charset FileName=CHARSET"
This error happens if you have filename with special symbols. This problem 
comes from Windows console. To avoid this problem rename file. If you have 
a lot of files, you can use script called `file_renamer.py`
