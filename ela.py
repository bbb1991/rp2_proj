import logging
import os

import cv2
import numpy as np
from PIL import Image, ImageChops
from skimage.measure import _structural_similarity as ssim

logger = logging.getLogger(__name__)
TMP_EXT = ".ELA_TMP.jpg"
EXT = ".jpg"
quality = 85
ErrorScale = 10


def calculate_difference_using_ssim_and_mse(original_image, compressed_image):
    compressed_image = cv2.imread(compressed_image)
    compressed_image = cv2.cvtColor(compressed_image, cv2.COLOR_BGR2GRAY)
    original_image = cv2.imread(original_image)
    original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2GRAY)

    def calculate_mse(oi, ci):
        error = np.sum((oi.astype("float") - ci.astype("float")) ** 2)
        error /= float(oi.shape[0] * oi.shape[1])
        return error

    MSE = calculate_mse(original_image, compressed_image)
    SSIM = ssim.compare_ssim(original_image, compressed_image)

    return {"SSIM": SSIM, "MSE": MSE}


def compute_ela(filename, original_directory, destination_directory):
    try:
        basename, ext = os.path.splitdrive(filename)
        original_filename = os.path.join(original_directory, filename)
        temporary_filename = os.path.join(destination_directory, filename[0:filename.index('.')] + basename + TMP_EXT)
        difference_image = destination_directory

        difference_image = os.path.join(difference_image,
                                        filename[0:filename.index('.')] + "." + basename + 'ela_difference' + EXT)

        original_image = Image.open(original_filename)
        original_image.save(temporary_filename, format='JPEG', quality=quality)
        temporary = Image.open(temporary_filename)
        difference = ImageChops.difference(original_image, temporary)
        d = difference.load()

        width, height = difference.size
        for x in range(width):
            for y in range(height):
                d[x, y] = tuple(k * ErrorScale for k in d[x, y])

        difference.save(difference_image, format='JPEG')
        result = calculate_difference_using_ssim_and_mse(original_filename, temporary_filename)
        return result
    except MemoryError:
        logger.error("MemoryError! Skipping ELA validation...")
    except Exception as e:
        logger.exception("Cannot parse image: {}".format(filename))
