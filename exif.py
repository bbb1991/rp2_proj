import re
from datetime import datetime

graphical_editors = [
    re.compile(r'adobe photoshop.*', re.IGNORECASE),
    re.compile(r'gimp.*', re.IGNORECASE),
    re.compile(r'paint.*', re.IGNORECASE),
    re.compile(r'.*photo editor.*', re.IGNORECASE),
    re.compile(r'.*photo viewer.*', re.IGNORECASE)
]

history_tags = [
    'HistoryWhen',
    'HistoryAction',
    'HistoryParameters',
]

photoshop_tags = [
    'SlicesGroupName',
    'TextLayerName',
    'TextLayerText'
]

DATETIME_TEMPLATE = '%Y:%m:%d %X'


def is_graphical_editor(software):
    if not software:
        return False

    if not isinstance(software, str):
        return False

    for graphical_editor in graphical_editors:
        if graphical_editor.match(software):
            return True
    return False


def validate_dates(create_date, modify_date):
    """
    2018:04:12 07:20:12+03:00
    :param create_date:
    :param modify_date:
    :return:
    """

    if not create_date or not modify_date:
        raise InvalidDateException(message="Required date is empty!")

    create_date = create_date[:19]
    modify_date = modify_date[:19]

    create_date = datetime.strptime(create_date, DATETIME_TEMPLATE)
    modify_date = datetime.strptime(modify_date, DATETIME_TEMPLATE)
    # max_allowed_date = datetime.now() - dt.timedelta(days=DATETIME_OFFSET)
    #
    # if create_date < max_allowed_date:
    #     raise InvalidDateException(message="Create date is older than max allowed date")

    if create_date > modify_date:
        raise InvalidDateException(
            message="Modify date is older than create date! modified: {}, created: {}".format(modify_date, create_date))


class InvalidDateException(BaseException):
    def __init__(self, *args, **kwargs):
        self.message = kwargs.get('message')


def parse_jpeg_snoop_log(log):
    lines = []
    regex = re.compile(r"(SW|NOTE|ASSESSMENT|CAM)")
    for line in log:
        if regex.search(line):
            lines.append(line)
    return lines


def extract_suspicious_fields(json_data, tags):
    data = dict()
    for tag in tags:
        if json_data.get(tag):
            data[tag] = json_data.get(tag)
    return data
