import argparse
import os

parser = argparse.ArgumentParser()

parser.add_argument('--dir', dest='directory', required=True, help='path to directory with scans')
"""
Bulk file renamer
To run it, type:

python file_renamer.py --dir /path/to/folder/with/files

"""
if __name__ == '__main__':
    counter = 1
    args = parser.parse_args()
    dirc = args.directory

    for f in [file for file in os.listdir(dirc)]:
        name, extension = os.path.splitext(f)

        src = os.path.join(dirc, f)
        dst = os.path.join(dirc, str(counter) + extension)

        os.rename(src, dst)
        counter += 1
