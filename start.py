import argparse
import json
import logging
import os
import re
import shutil
import tempfile
from subprocess import Popen, PIPE

import ela
import exif
import settings


class ResultsEnum:
    ORIGINAL = "Original scan"
    MAYBE = "Maybe"
    AMBIGIOUS = "Ambigious result"
    FAKE = "Fake scan"


logging.basicConfig(level=logging.DEBUG if settings.DEBUG else logging.INFO,
                    format=u'%(filename)10s:%(lineno)d # %(levelname)-8s [%(asctime)s]  %(message)s')
logger = logging.getLogger(__name__)
fileHandler = logging.FileHandler("output.log", mode='w')
logger.addHandler(fileHandler)

parser = argparse.ArgumentParser()

parser.add_argument('--dir', dest='directory', required=True, help='path to directory with scans')

exiftool = os.path.join("resources", "exiftool")
jpegsnoop = os.path.join("resources", "JPEGsnoop")


def get_process_output(*args):
    process = Popen(list(args), stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()

    if exit_code != 0:
        raise AssertionError("Exit code is not 0! args: {}, exit code: {}".format(args, exit_code))
    return output.decode('utf-8')


def extract_class(lines):
    assessment = re.findall("Class \\d", lines)
    return assessment[0] if assessment else None


def get_result(jpeg_snoop_class, mse, ssim):
    if jpeg_snoop_class == 'Class 3':
        if mse < 0.6 and ssim > 0.96:
            return ResultsEnum.ORIGINAL
        else:
            return ResultsEnum.AMBIGIOUS
    elif jpeg_snoop_class == 'Class 4':
        if mse < 1 < ssim:
            return ResultsEnum.MAYBE
        else:
            return ResultsEnum.AMBIGIOUS
    elif jpeg_snoop_class in ['Class 1', 'Class 2']:
        if mse > 1 and ssim > 1:
            return ResultsEnum.FAKE
        else:
            return ResultsEnum.AMBIGIOUS
    elif jpeg_snoop_class in ['Class 1', 'Class 2']:
        if mse < 1 and ssim < 1:
            return ResultsEnum.FAKE
        else:
            return ResultsEnum.AMBIGIOUS


if __name__ == '__main__':
    args = parser.parse_args()
    dirc = args.directory

    temp_dir = os.path.join(tempfile.gettempdir(), 'ela')

    if os.path.exists(temp_dir):
        shutil.rmtree(temp_dir, ignore_errors=True)
    os.mkdir(temp_dir)

    for f in [file for file in os.listdir(dirc) if file.endswith('.jpg') or file.endswith('.jpeg')]:
        score = 0
        file = os.path.join(dirc, f)

        logger.debug("==================================================================")
        logger.debug("Processing file: {}".format(f))

        # Analyze using JPEGSnoop
        get_process_output(jpegsnoop, '-i', file, '-o', os.path.join(temp_dir, f + '.snoop'))

        jpeg_snoop_class = None
        snoop_output_file = None
        try:
            snoop_output_file = open(os.path.join(temp_dir, f + '.snoop'))
            lines = snoop_output_file.readlines()
            lines = [l.strip() for l in lines if l.strip()][-10:]
            logger.debug("=== JPEG Snoop log analysis begin ===")
            lines = exif.parse_jpeg_snoop_log(lines)
            lines = "\n".join(lines)
            logger.debug("\n" + lines)
            if len(re.findall(r"SW", lines)) > 1:
                score += 1
            jpeg_snoop_class = extract_class(lines)
            logger.debug("=== JPEG Snoop log analysis end ===")
        except UnicodeDecodeError:
            logger.error("Error while parsing JPEGSnoop output! Skipping JPEGSnoop validation...")
        finally:
            snoop_output_file.close()

        # Analyze metadata
        exiftool_result = get_process_output(exiftool, '-j', file)
        exiftool_result = json.loads(exiftool_result)[0]

        non_standard_data = {}
        history_tags_result = exif.extract_suspicious_fields(exiftool_result, exif.history_tags) or {}

        if history_tags_result:
            score += 1

        photoshop_tags_result = exif.extract_suspicious_fields(exiftool_result, exif.photoshop_tags) or {}

        if photoshop_tags_result:
            score += 2

        non_standard_data.update(photoshop_tags_result)
        non_standard_data.update(history_tags_result)

        for k, v in non_standard_data.items():
            logger.debug("EXIF contains extra data: key: {}, value: {}".format(k, v))

        software = exiftool_result.get('Software')
        create_date = exiftool_result.get('FileCreateDate')
        modify_date = exiftool_result.get('FileModifyDate')

        if exif.is_graphical_editor(software):
            logger.debug("Graphical editor tool detected: {}".format(software))

        try:
            exif.validate_dates(create_date, modify_date)
        except exif.InvalidDateException as e:
            score += 1
            logger.debug(e.message)

        # Analyze ELA
        ela_result = ela.compute_ela(f, dirc, temp_dir)
        logger.debug("ELA result: {}".format(ela_result))

        result = None

        if ela_result and jpeg_snoop_class:
            mse = ela_result.get('MSE')
            ssim = ela_result.get('SSIM')

            logger.debug("JPEG Snoop class: {}".format(jpeg_snoop_class))

            result = get_result(jpeg_snoop_class, mse, ssim)
            if result == ResultsEnum.AMBIGIOUS:
                if score >= 2:
                    result = ResultsEnum.FAKE

        else:
            if score >= 2:
                result = ResultsEnum.FAKE

        if result:
            logger.info("Result for file {} is: {}".format(f, result))
        else:
            logger.info("Cannot validate result! Some of steps failed!")
